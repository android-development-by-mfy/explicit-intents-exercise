package com.mfy.explicitintentsexercise;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SurnameActivity extends AppCompatActivity {

    EditText et_surname;
    Button b_saveSurname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surname);

        et_surname = findViewById(R.id.et_surname);
        b_saveSurname = findViewById(R.id.b_saveSurname);

        final String surname = et_surname.getText().toString().trim();

        b_saveSurname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (surname.isEmpty()) {
                    Toast.makeText(SurnameActivity.this, getString(R.string.fillAll), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent();
                intent.putExtra("surname", surname);
                setResult(RESULT_OK, intent);
                SurnameActivity.this.finish();
            }
        });
    }
}
