package com.mfy.explicitintentsexercise;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button b_name, b_surname, b_birthYear, b_seeSaved;

    final int ACTIVITY_NAME = 1;
    final int ACTIVITY_SURNAME = 2;
    final int ACTIVITY_BIRTH_YEAR = 3;

    String name = "not set yet";
    String surname = "not set yet";
    int birthYear = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_name = findViewById(R.id.b_name);
        b_surname = findViewById(R.id.b_surname);
        b_birthYear = findViewById(R.id.b_birthYear);
        b_seeSaved = findViewById(R.id.b_seeSaved);

        b_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, com.mfy.explicitintentsexercise.NameActivity.class);
                startActivityForResult(intent, ACTIVITY_NAME);
            }
        });

        b_surname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, com.mfy.explicitintentsexercise.SurnameActivity.class);
                startActivityForResult(intent, ACTIVITY_SURNAME);
            }
        });

        b_birthYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_NAME) {
            if (resultCode == RESULT_OK) {
                name = getIntent().getStringExtra(name);
            }
        }
        else if (requestCode == ACTIVITY_SURNAME) {}
        else {}
    }
}
