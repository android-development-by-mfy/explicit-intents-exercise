package com.mfy.explicitintentsexercise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NameActivity extends AppCompatActivity {

    EditText et_name;
    Button b_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        et_name = findViewById(R.id.et_surname);
        b_save = findViewById(R.id.b_saveSurname);

        final String name = et_name.getText().toString().trim();

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.isEmpty()) {
                    Toast.makeText(NameActivity.this, getString(R.string.fillAll), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent();
                intent.putExtra("name", name);
                setResult(RESULT_OK, intent);
                NameActivity.this.finish();
            }
        });
    }
}
